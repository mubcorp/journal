<?php

namespace app\migrations;
use app\commands\Migration;

class m180319_065145_contact_form extends Migration
{
    public function getTableName()
    {
        return 'contact_form';
    }

    public function getKeyFields()
    {
        return [
            'name'  =>  'name',
            'email'  =>  'email',
            'mobile' => 'mobile'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'sur_name' => $this->string(100)->notNull(),
            'designation' => $this->string(100),
            'department' => $this->string(100),
            'organisation' => $this->string(100),
            'address' => $this->string(100)->notNull(),
            'city' => $this->string(100)->notNull(),
            'location' => $this->string(100)->notNull(),
            'state' => $this->string(100)->notNull(),
            'pincode' => $this->string(100),
            'country' => $this->string(100)->notNull(),
            'email' => $this->string(100)->notNull(),
            'telephone' => $this->string(100),
            'mobile' => $this->string(100)->notNull(),
            'website' => $this->string(100),
            'contact_person' => $this->string(100),
            'message' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
