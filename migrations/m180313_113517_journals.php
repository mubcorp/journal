<?php

namespace app\migrations;
use app\commands\Migration;

class m180313_113517_journals extends Migration

{
    public function getTableName()
    {
        return 'journals';
    }

    public function getKeyFields()
    {
        return [
            'print_issn'  =>  'print_issn',
            'online_issn'  =>  'online_issn'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'title' => "mediumtext NOT NULL",
            'slug' => "mediumtext NOT NULL",
            'category' =>  "enum('INTERNATIONAL','NATIONAL','NA')",
            'print_issn' => $this->string(100),
            'online_issn' => $this->string(100),
            'frequency' => $this->string(100)->notNull(),
            'periodicity' => $this->string(100)->notNull(),
            'format' => $this->string(255)->notNull(),
            'language' => $this->string(100)->notNull(),
            'origin' => $this->string(100)->notNull(),
            'currency' => $this->string(255)->notNull(),
            'price' => $this->string(255)->notNull(),
            'exchange_rate' => $this->string(255)->notNull(),
            'inr_price' => $this->string(255)->notNull(),
            'publisher' => $this->string(100)->notNull(),
            'description' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
