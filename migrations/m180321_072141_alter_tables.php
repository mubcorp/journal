<?php

namespace app\migrations;
use app\commands\Migration;

class m180321_072141_alter_tables extends Migration
{

    public function getTableName()
    {
        return 'journals';
    }

    public function getKeyFields()
    {
        return [
            'print_issn'  =>  'print_issn',
            'online_issn'  =>  'online_issn'
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $file = __DIR__.'/alter.sql';
        if(file_exists($file)){
          $sql = file_get_contents($file);
          $this->execute($sql);    
        }
        else{
            p($file);
        }
    }


    public function getFields()
    {
        return [
            ];
    }

    public function safeDown()
    {
        echo "m180321_072141_alter_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_072141_alter_tables cannot be reverted.\n";

        return false;
    }
    */
}
