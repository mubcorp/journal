<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;?>
	<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					<br/><br/>
						<!-- <div class="banner-text">
							<h1 class="page-title">Contact FinanceCorp</h1>
							<p>Would you like to come by and say hi?</p>						
						</div> -->
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="/">Home</a></li>
								<li class="active"><span>Contact Us</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<!-- <div class="banner-bg imagebg">
				<img src="/images/conat.jpg" alt="" />
			</div> -->
		</div>
		<!-- #end Banner/Static -->
	</header>
	<div class="section section-contents section-contact section-pad">
		<div class="container">
			<div class="content row">

				<h2 class="heading-lg">Contact Us</h2>
				<div class="contact-content row">
					<div class="drop-message col-md-7 res-m-bttm">
						
						<form class="form-quote" action="mailto:samar@core3online.com" method="post" enctype="text/plain">
								<div class="form-group row">
									<div class="form-field col-md-6 form-m-bttm">
										<input name="quote-request-name" type="text" placeholder="Name *" class="form-control required">
									</div>
									<div class="form-field col-md-6">
										<input name="quote-request-company" type="text" placeholder="Company" class="form-control">
									</div>
								</div>
								<div class="form-group row">
									<div class="form-field col-md-6 form-m-bttm">
										<input name="quote-request-email" type="email" placeholder="Email *" class="form-control required email">
									</div>
									<div class="form-field col-md-6">
										<input name="quote-request-phone" type="text" placeholder="Phone *" class="form-control required">
									</div>
								</div>
								
								
								<div class="form-group row">
									<div class="form-field col-md-12">
										<textarea name="quote-request-message" placeholder="Messages *" class="txtarea form-control required"></textarea>
									</div>
								</div>
								
								<input type="submit" class="btn" value="Send">
								
							</form>
					</div>
					<div class="contact-details col-md-4 col-md-offset-1">
						<ul class="contact-list">
							<li><em class="fa fa-map" aria-hidden="true"></em>
						    <span>Unit 706, 7/F, South Seas Centre, Tower 2, 75 Mody Road, Tsimshatsui, Kowloon, Hong Kong
.</span>
							</li>
							<li><em class="fa fa-phone" aria-hidden="true"></em>
								<span>Telephone : (852) 3913 9545<br>FAX : (852) 3743 5035</span>
							</li>
							<li><em class="fa fa-envelope" aria-hidden="true"></em>
								<span>Email : <a href="#">samar@core3online.com</a></span>
							</li>
							<li>
								<em class="fa fa-clock-o" aria-hidden="true"></em><span>Sat - Thu: 8AM - 7PM </span>
							</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- End Content -->

	<!-- Map -->
	<div class="map-holder map-contact">
		<div id="gmap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29531.417144814175!2d114.16118843736032!3d22.299679885323823!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x340400ef70fc03ad%3A0xcad0bd32ef71ce72!2sSouth+Seas+Centre%2C+75+Mody+Rd%2C+Tsim+Sha+Tsui+East%2C+Hong+Kong!5e0!3m2!1sen!2sin!4v1512473111632" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
	</div><br/>
	<!-- End map -->	
	<!-- Client logo -->
	