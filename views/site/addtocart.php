
<?php $cartItems = $cart->getPositions();
	$count = count ($cartItems);
?>
<div class="table-cont">
  <table id='table-national'>
	<tr class="items-head">
		<th class="numbering">ID</th>
		<th class="name-id">Title</th>
		<th class="frequency">Qty</th>
		<th class="periodicity">Price</th>
		<th></th>
	</tr>
	<tr class="like-to-subscribe">
    	<td colspan="10"><div class="col-xs-12 pad-none info-selected">We would like to Subscribe!</div></td>
	</tr>
	<tr class="table-heading">
		<td colspan="10">National</td>
	</tr>
	 <?php           
      $total = $cart->getCost();
      if(!empty($cartItems))
      {   $j = 0;              
          foreach($cartItems as $key => $cartItem){
          	if($cartItem->category == 'NATIONAL'){
          		$j++;
          	?>
			<tr>
				<td class="numbering"><?= $j;?></td>
				<td class="name-id"><?= $cartItem->title;?></td>
				<td class="frequency"><?= $cartItem->getQuantity();?> x</td>
				<td class="periodicity"><?= $cartItem->inr_price;?></td>
				<td> <a class="btn-remove-item" id="remove_item_<?= $cartItem->id; ?>"><i class="fa fa-trash pull-right"></i>
                </a></td>
			</tr>
	  <?php }}}?>	
  </table>
  </div><br/>
  <div class="table-cont">
	  <table id='table-international'>
		<tr class="table-heading">
			<td colspan="10">International</td>
		</tr>
		<?php           
          $total = $cart->getCost(); 
          if(!empty($cartItems))
          { $i = 0;
            foreach($cartItems as $key => $cartItem){
            	if($cartItem->category == 'INTERNATIONAL'){
            		$i++;
            	?>
				<tr>
					<td class="numbering"><?= $i;?></td>
					<td class="name-id"><?= $cartItem->title;?></td>
					<td class="frequency"><?= $cartItem->getQuantity();?> x</td>
					<td class="periodicity"><?= $cartItem->inr_price;?></td>
					<td> <a class="btn-remove-item" id="remove_item_<?= $cartItem->id; ?>"><i class="fa fa-trash pull-right"></i>
                          </a></td>
				</tr>
		  <?php }}}?>	
	  </table>
  </div>
  <div class="col-xs-12 pad-none cont-total">
	<div class="selected-items total-titles">Total Titles Selected : <?=$count?></div>
	<div class="selected-items total-inr">Total INR : <?= $cart->getCost()?></div>
  </div>