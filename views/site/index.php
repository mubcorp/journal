<?php  

	use yii\helpers\Html;
    use yii\widgets\ActiveForm;
	use yz\shoppingcart\ShoppingCart;

    $cart = new ShoppingCart();
    $cartItems = $cart->getPositions();
    $count = count ( $cartItems );
?>

<div class="col-xs-12 cont-content">
  <section class="national-international">
	<h2>Subscribe Nursing Journals & Magazines</h2>
	<h4>National & International</h4>
	<div class="col-xs-12 pad-none steps-easy">
	  <div class="outer-steps-list">
		<div class="steps-list easy-steps-tab"><a href="javascript:;">View</a></div>
		<div class="steps-list"><a href="javascript:;">Select</a></div>
		<div class="steps-list"><a href="javascript:;">Shortlist</a></div>
		<div class="steps-list"><a href="javascript:;">Email</a></div>
		<div class="steps-list"><a href="javascript:;">Get Quote</a></div>
		<div class="steps-list"><a href="javascript:;">Approve</a></div>
		<div class="steps-list"><a href="javascript:;">Subscribe</a></div>
		<div class="steps-list"><a href="javascript:;">Renew</a></div>
	  </div>
	</div>
  </section>
  
  <section class="slider-part">
	<div class="col-sm-12 col-md-12 col-xs-12 pade_none otr-slider">
      <div id="demo" class="slider_top_paddn">
        <div id="owl-demo" class="owl-carousel">
          <div class="item"><a href="javascript:;"><img src="images/item-img-01.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-02.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-03.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-04.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-05.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-06.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-07.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-01.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-02.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-03.png" alt="Image"></a></div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="sorting-part">
	<div class="col-md-8 col-sm-8 col-xs-12 sorting-left-side">
	  <div id="tabs-container">
		<ul class="tabs-menu">
			<li class="current"><a href="#tab-1">Price<span>Low - High</span></a></li>
			<li><a href="#tab-2">Price<span>High - Low</span></a></li>
			<li><a href="#tab-3">Title<span>A - Z</span></a></li>
			<li><a href="#tab-4">Title<span>Z - A</span></a></li>
		</ul>
		<div class="tab">
		  <div id="tab-1" class="tab-content">
			<div class="table-cont">
			  <table>
				<tr class="items-head">
					<th class="numbering">No</th>
					<th class="name-id">Title</th>
					<th class="frequency">Frequency</th>
					<th class="periodicity">Periodicity</th>
					<th class="print">Format</th>
					<th class="inr">Currency</th>
					<th class="numeric">Price</th>
					<th class="exchange-rate">Exchange<br> Rate</th>
					<th class="numeric-2">INR Price</th>
					<th class="checked">Select</th>
				</tr>
				<tr class="table-heading">
					<td colspan="10">National Nursing Journals</td>
				</tr>
				<?php 
				foreach($journals['priceLowNational'] as $key => $nJournal){
					$inCart = $cart->getPositionById($nJournal->id);
					
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$nJournal->title;?></td>
					<td class="frequency"><?=$nJournal->frequency;?></td>
					<td class="periodicity"><?=$nJournal->periodicity;?></td>
					<td class="print"><?=$nJournal->format;?></td>
					<td class="inr"><?=$nJournal->currency;?></td>
					<td class="numeric"><?=$nJournal->price;?></td>
					<td class="exchange-rate"><?=$nJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$nJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" class="<?=($inCart)? 'selected' : '';?>" id="c_<?=$nJournal->id;?>_l" name="cc" <?=($inCart)? 'checked' : '';?>/> 
						<label class="btn-addcart" for="c_<?=$nJournal->id;?>_l"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  <!--/table>
			  <table-->
				<tr class="table-heading">
					<td colspan="10">International Nursing Journals</td>
				</tr>
				<?php foreach($journals['priceLowInternational'] as $key => $intJournal){
					$inCart = $cart->getPositionById($intJournal->id);
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$intJournal->title;?></td>
					<td class="frequency"><?=$intJournal->frequency;?></td>
					<td class="periodicity"><?=$intJournal->periodicity;?></td>
					<td class="print"><?=$intJournal->format;?></td>
					<td class="inr"><?=$intJournal->currency;?></td>
					<td class="numeric"><?=$intJournal->price;?></td>
					<td class="exchange-rate"><?=$intJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$intJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" class="<?=($inCart)? 'selected' : '';?>" id="i_<?=$intJournal->id;?>_l" name="cc" <?=($inCart)? 'checked' : '';?>/>
						<label class="btn-addcart" for="i_<?=$intJournal->id;?>_l"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  </table>
			</div>
		  </div>

		  <div id="tab-2" class="tab-content">
			<div class="table-cont">
			  <table>
				<tr class="items-head">
					<th class="numbering">No</th>
					<th class="name-id">Title</th>
					<th class="frequency">Frequency</th>
					<th class="periodicity">Periodicity</th>
					<th class="print">Format</th>
					<th class="inr">Currency</th>
					<th class="numeric">Price</th>
					<th class="exchange-rate">Exchange<br> Rate</th>
					<th class="numeric-2">INR Price</th>
					<th class="checked">Select</th>
				</tr>
				<tr class="table-heading">
					<td colspan="10">National Nursing Journals</td>
				</tr>
				<?php 
				foreach($journals['priceNational'] as $key => $nJournal){
					$inCart = $cart->getPositionById($nJournal->id);
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$nJournal->title;?></td>
					<td class="frequency"><?=$nJournal->frequency;?></td>
					<td class="periodicity"><?=$nJournal->periodicity;?></td>
					<td class="print"><?=$nJournal->format;?></td>
					<td class="inr"><?=$nJournal->currency;?></td>
					<td class="numeric"><?=$nJournal->price;?></td>
					<td class="exchange-rate"><?=$nJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$nJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" class="<?=($inCart)? 'selected' : '';?>" id="c_<?=$nJournal->id;?>_h" name="cc" <?=($inCart)? 'checked' : '';?>/>
						<label class="btn-addcart" for="c_<?=$nJournal->id;?>_h"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  <!--/table>
			  <table-->
				<tr class="table-heading">
					<td colspan="10">International Nursing Journals</td>
				</tr>
				<?php foreach($journals['priceInternational'] as $key => $intJournal){
					$inCart = $cart->getPositionById($intJournal->id);
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$intJournal->title;?></td>
					<td class="frequency"><?=$intJournal->frequency;?></td>
					<td class="periodicity"><?=$intJournal->periodicity;?></td>
					<td class="print"><?=$intJournal->format;?></td>
					<td class="inr"><?=$intJournal->currency;?></td>
					<td class="numeric"><?=$intJournal->price;?></td>
					<td class="exchange-rate"><?=$intJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$intJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" id="i_<?=$intJournal->id;?>_h" name="cc" class="<?=($inCart)? 'selected' : '';?>" <?=($inCart)? 'checked' : '';?>/>
						<label class="btn-addcart" for="i_<?=$intJournal->id;?>_h"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  </table>
			</div>
		  </div>

		  <div id="tab-3" class="tab-content">
			<div class="table-cont">
			  <table>
				<tr class="items-head">
					<th class="numbering">No</th>
					<th class="name-id">Title</th>
					<th class="frequency">Frequency</th>
					<th class="periodicity">Periodicity</th>
					<th class="print">Format</th>
					<th class="inr">Currency</th>
					<th class="numeric">Price</th>
					<th class="exchange-rate">Exchange<br> Rate</th>
					<th class="numeric-2">INR Price</th>
					<th class="checked">Select</th>
				</tr>
				<tr class="table-heading">
					<td colspan="10">National Nursing Journals</td>
				</tr>
				<?php 
				foreach($journals['national'] as $key => $nJournal){
					$inCart = $cart->getPositionById($nJournal->id);
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$nJournal->title;?></td>
					<td class="frequency"><?=$nJournal->frequency;?></td>
					<td class="periodicity"><?=$nJournal->periodicity;?></td>
					<td class="print"><?=$nJournal->format;?></td>
					<td class="inr"><?=$nJournal->currency;?></td>
					<td class="numeric"><?=$nJournal->price;?></td>
					<td class="exchange-rate"><?=$nJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$nJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" id="c_<?=$nJournal->id;?>_a" name="cc" class="<?=($inCart)? 'selected' : '';?>" <?=($inCart)? 'checked' : '';?>/>
						<label class="btn-addcart" for="c_<?=$nJournal->id;?>_a"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  <!--/table>
			  <table-->
				<tr class="table-heading">
					<td colspan="10">International Nursing Journals</td>
				</tr>
				<?php foreach($journals['international'] as $key => $intJournal){
					$inCart = $cart->getPositionById($intJournal->id);
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$intJournal->title;?></td>
					<td class="frequency"><?=$intJournal->frequency;?></td>
					<td class="periodicity"><?=$intJournal->periodicity;?></td>
					<td class="print"><?=$intJournal->format;?></td>
					<td class="inr"><?=$intJournal->currency;?></td>
					<td class="numeric"><?=$intJournal->price;?></td>
					<td class="exchange-rate"><?=$intJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$intJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" id="i_<?=$intJournal->id;?>_a" name="cc" class="<?=($inCart)? 'selected' : '';?>" <?=($inCart)? 'checked' : '';?>/>
						<label class="btn-addcart" for="i_<?=$intJournal->id;?>_a"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  </table>
			</div>
		  </div>

		  <div id="tab-4" class="tab-content">
			<div class="table-cont">
			  <table>
				<tr class="items-head">
					<th class="numbering">No</th>
					<th class="name-id">Title</th>
					<th class="frequency">Frequency</th>
					<th class="periodicity">Periodicity</th>
					<th class="print">Format</th>
					<th class="inr">Currency</th>
					<th class="numeric">Price</th>
					<th class="exchange-rate">Exchange<br> Rate</th>
					<th class="numeric-2">INR Price</th>
					<th class="checked">Select</th>
				</tr>
				<tr class="table-heading">
					<td colspan="10">National Nursing Journals</td>
				</tr>
				<?php 
				foreach($journals['desNational'] as $key => $nJournal){
					$inCart = $cart->getPositionById($nJournal->id);
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$nJournal->title;?></td>
					<td class="frequency"><?=$nJournal->frequency;?></td>
					<td class="periodicity"><?=$nJournal->periodicity;?></td>
					<td class="print"><?=$nJournal->format;?></td>
					<td class="inr"><?=$nJournal->currency;?></td>
					<td class="numeric"><?=$nJournal->price;?></td>
					<td class="exchange-rate"><?=$nJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$nJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" id="c_<?=$nJournal->id;?>_z" name="cc" class="<?=($inCart)? 'selected' : '';?>" <?=($inCart)? 'checked' : '';?>/>
						<label class="btn-addcart" for="c_<?=$nJournal->id;?>_z"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  <!--/table>
			  <table-->
				<tr class="table-heading">
					<td colspan="10">International Nursing Journals</td>
				</tr>
				<?php foreach($journals['desInternational'] as $key => $intJournal){
					$inCart = $cart->getPositionById($intJournal->id);
					?>
				<tr>
					<td class="numbering"><?=$key+1;?></td>
					<td class="name-id"><?=$intJournal->title;?></td>
					<td class="frequency"><?=$intJournal->frequency;?></td>
					<td class="periodicity"><?=$intJournal->periodicity;?></td>
					<td class="print"><?=$intJournal->format;?></td>
					<td class="inr"><?=$intJournal->currency;?></td>
					<td class="numeric"><?=$intJournal->price;?></td>
					<td class="exchange-rate"><?=$intJournal->exchange_rate;?></td>
					<td class="numeric-2"><?=$intJournal->inr_price;?></td>
					<td class="checked">
					  <div class="box_check_txt">
						<input type="checkbox" id="i_<?=$intJournal->id;?>_z" name="cc" class="<?=($inCart)? 'selected' : '';?>" <?=($inCart)? 'checked' : '';?>/>
						<label class="btn-addcart" for="i_<?=$intJournal->id;?>_z"><span class="green"></span></label>
					  </div>
					</td>
				</tr>
				<?php }?>
			  </table>
			</div>
		  </div>

		</div>
	  </div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 sorting-right-side">
	  <div class="col-xs-12 pad-none sort-options">
	    <div class="checkbox-center">
			<div class="box_check_txt">
				<input type="checkbox" id="opt1" name="cc" />
				<label for="opt1"><span class="green"></span>National</label>
			</div>
			<div class="box_check_txt">
				<input type="checkbox" id="opt2" name="cc" />
				<label for="opt2"><span class="green"></span>International</label>
			</div>
			<div class="box_check_txt">
				<input type="checkbox" id="opt3" name="cc" />
				<label for="opt3"><span class="green"></span>Print List</label>
			</div>
		</div>
	  </div>
	  <div class="col-xs-12 pad-none right-small-table" id="cart">
	  <div  id='dynamic-cart'>
		  <div class="table-cont">
			  <table id='table-national'>
				<tr class="items-head">
					<th class="numbering">ID</th>
					<th class="name-id">Title</th>
					<th class="frequency">Qty</th>
					<th class="periodicity">Price</th>
					<th></th>
				</tr>
				<tr class="like-to-subscribe">
                	<td colspan="10"><div class="col-xs-12 pad-none info-selected">We would like to Subscribe!</div></td>
				</tr>
				<tr class="table-heading">
					<td colspan="10">National</td>
				</tr>
				 <?php           
                  $total = $cart->getCost();
                  if(!empty($cartItems))
                  { 
                  	$j = 0; 
                      foreach($cartItems as $cartItem){
                      	if($cartItem->category == 'NATIONAL'){
                      		$j++;
                      	?>
						<tr>
							<td class="numbering"><?= $j;?></td>
							<td class="name-id"><?= $cartItem->title;?></td>
							<td class="frequency"><?= $cartItem->getQuantity();?> x</td>
							<td class="periodicity"><?= $cartItem->inr_price;?></td>
							<td> <a class="btn-remove-item" id="remove_item_<?= $cartItem->id; ?>"><i class="fa fa-trash pull-right"></i>
                                  </a></td>
						</tr>
				  <?php }}}?>	
			  </table>
		  </div><br/>
		  <div class="table-cont">
			  <table id='table-international'>
				<tr class="table-heading">
					<td colspan="10">International</td>
				</tr>
				<?php           
                  $total = $cart->getCost(); 
                  if(!empty($cartItems))
                  { $i =0;
                    foreach($cartItems as $cartItem){
                    	if($cartItem->category == 'INTERNATIONAL'){
                    		$i++;
                    	?>
						<tr>
							<td class="numbering"><?= $i;?></td>
							<td class="name-id"><?= $cartItem->title;?></td>
							<td class="frequency"><?= $cartItem->getQuantity();?>x</td>
							<td class="periodicity"><?= $cartItem->inr_price;?></td>
							<td> <a class="btn-remove-item" id="remove_item_<?= $cartItem->id; ?>"><i class="fa fa-trash pull-right"></i>
                                  </a></td>
						</tr>
				  <?php }}}?>	
			  </table>
		  </div>
		  <div class="col-xs-12 pad-none cont-total">
			<div class="selected-items total-titles">Total Titles Selected : <?=$count?></div>
			<div class="selected-items total-inr">Total INR : <?= $cart->getCost()?></div>
		  </div>
		  </div>
		  <div class="col-xs-12 cont-detail-form">
			<span>Please fill your</span>
			<h3>Details &amp; Send</h3>
			<div class="col-xs-12 pad-none detail-form-otr">
			 <?php $form = ActiveForm::begin(['id' => 'contact_office', 'method' => 'post']); ?>
				<form type="" method="">
					<div class="cont-fields">
						<div class="one-half"><?= $form->field($contact, 'name')->textInput(['maxlength' => 30, 'placeholder' => 'First Name','class' => 'one-half'])->label(false);?></div>
						<div class="second-half"><?= $form->field($contact, 'sur_name')->textInput(['maxlength' => 30, 'placeholder' => 'Surname','class' => 'second-half'])->label(false);?></div>
					</div>
					<div class="cont-fields">
						<div class="one-half"><?= $form->field($contact, 'designation')->textInput(['maxlength' => 30, 'placeholder' => 'Designation','class' => 'one-half'])->label(false);?></div>
						<div class="second-half"><?= $form->field($contact, 'department')->textInput(['maxlength' => 30, 'placeholder' => 'Department','class' => 'second-half'])->label(false);?></div>
					</div>
					<div class="cont-fields">
						<?= $form->field($contact, 'organisation')->textInput(['maxlength' => 30, 'placeholder' => 'Organisation','class' => 'second-half'])->label(false);?>
					</div>
					<div class="cont-fields">
						<?= $form->field($contact, 'address')->textInput(['maxlength' => 30, 'placeholder' => 'Address','class' => 'second-half'])->label(false);?>
					</div>
					<div class="cont-fields">
						<div class="one-half"><?= $form->field($contact, 'location')->textInput(['maxlength' => 30, 'placeholder' => 'Location','class' => 'one-half'])->label(false);?></div>
						<div class="second-half"><?= $form->field($contact, 'city')->textInput(['maxlength' => 30, 'placeholder' => 'City','class' => 'second-half'])->label(false);?></div>
					</div>
					<div class="cont-fields">
						<div class="one-half"><?= $form->field($contact, 'pincode')->textInput(['maxlength' => 30, 'placeholder' => 'Pincode','class' => 'one-half'])->label(false);?></div>
						<div class="second-half"><?= $form->field($contact, 'state')->textInput(['maxlength' => 30, 'placeholder' => 'State','class' => 'second-half'])->label(false);?></div>
					</div>
					<div class="cont-fields">
						<div class="one-half"><?= $form->field($contact, 'country')->textInput(['maxlength' => 30, 'placeholder' => 'Country','class' => 'one-half'])->label(false);?></div>
						<div class="second-half"><?= $form->field($contact, 'telephone')->textInput(['maxlength' => 30, 'placeholder' => 'Telephone','class' => 'second-half'])->label(false);?></div>
					</div>
					<div class="cont-fields">
						<div class="one-half"><?= $form->field($contact, 'email')->textInput(['maxlength' => 30, 'placeholder' => 'Email','class' => 'one-half'])->label(false);?></div>
						<div class="second-half"><?= $form->field($contact, 'mobile')->textInput(['maxlength' => 30, 'placeholder' => 'Mobile','class' => 'second-half'])->label(false);?></div>
					</div>
					<div class="cont-fields">
						<div class="one-half"><?= $form->field($contact, 'website')->textInput(['maxlength' => 30, 'placeholder' => 'Website','class' => 'one-half'])->label(false);?></div>
						<div class="second-half"><?= $form->field($contact, 'contact_person')->textInput(['maxlength' => 30, 'placeholder' => 'Contact Person','class' => 'second-half'])->label(false);?></div>
					</div>
					<div class="cont-fields">
						<?= $form->field($contact, 'message')->textarea(['placeholder' => 'Message'])->label(false);?>
					</div>					
					<div class="cont-submit">
						<?= Html::submitButton($contact->isNewRecord ? 'Submit' : 'Update', ['class' => $contact->isNewRecord ? 'cont-submit' : 'cont-submit']);?>
					</div>
				</form>
			<?php ActiveForm::end(); ?>
			</div>
		  </div>
		  <div class="col-xs-12 pad-none best-seller">
			<h3>About Nursing</h3>
            <p>Nursing is a profession within the health care sector focused on the care of individuals, families, and communities so they may attain, maintain, or recover optimal health and quality of life. Nurses may be differentiated from other health care providers by their approach to patient care, training, and scope of practice. Nurses practice in many specialties with differing levels of prescription authority. Many nurses provide care within the ordering scope of physicians, and this traditional role has shaped the public image of nurses as care providers. However, nurse practitioners are permitted by most jurisdictions to practice independently in a variety of settings. In the postwar period, nurse education has undergone a process of diversification towards advanced and specialized credentials, and many of the traditional regulations and provider roles are changing.</p>
            <p>Nurses develop a plan of care, working collaboratively with physicians, therapists, the patient, the patient's family and other team members, that focuses on treating illness to improve quality of life. In the United States and the United Kingdom, advanced practice nurses, such as clinical nurse specialists and nurse practitioners, diagnose health problems and prescribe medications and other therapies, depending on individual state regulations. Nurses may help coordinate the patient care performed by other members of a multidisciplinary health care team such as therapists, medical practitioners and dietitians. Nurses provide care both interdependently, for example, with physicians, and independently as nursing professionals.</p>
		  </div>
		  <div class="col-xs-12 pad-none best-seller nursing-specialities">
			<h3>Nursing Specialities</h3>
			<ul>
				<li>Ambulatory Care Nursing</li>
				<li>Advanced Practice Nursing</li>
				<li>Burn Nursing</li>
				<li>Camp Nursing</li>
				<li>Cardiac Nursing</li>
				<li>Cardiac Intervention Nursing</li>
				<li>Dental Nursing</li>
				<li>Medical Case Management</li>
				<li>Community Health Nursing</li>
				<li>Correctional Nursing</li>
				<li>Critical Care Nursing</li>
				<li>Emergency Nursing</li>
				<li>Environmental Health Nursing</li>
				<li>Faith Community Nursing</li>
				<li>Flight Nursing</li>
				<li>Forensic Nursing</li>
				<li>Gastroenterology Nursing</li>
				<li>Genetics Nursing</li>
				<li>Geriatric Nursing</li>
				<li>Health Visiting</li>
				<li>Holistic Nursing</li>
				<li>Home Health Nursing</li>
				<li>Hospice and Palliative Care Nursing</li>
				<li>Hyperbaric Nursing</li>
				<li>Immunology and Allergy Nursing</li>
				<li>Intravenous Therapy Nursing</li>
				<li>Infection Control Nursing</li>
				<li>Infectious Disease Nursing</li>
				<li>Legal Nurse Consultant</li>
				<li>Maternal-Child Nursing</li>
				<li>Medical-Surgical Nursing</li>
				<li>Military and Uniformed Services Nursing</li>
				<li>Neonatal Nursing</li>
				<li>Neurosurgical Nursing</li>
				<li>Nephrology Nursing</li>
				<li>Nurse Attorney</li>
				<li>Nursing Informatics</li>
				<li>Nursing Management</li>
				<li>Nursing Research</li>
				<li>Nurse Midwifery</li>
				<li>Obstetrical Nursing</li>
				<li>Occupational Health Nursing</li>
				<li>Oncology Nursing</li>
				<li>Orthopaedic Nursing</li>
				<li>Ostomy Nursing</li>
				<li>Pediatric Nursing</li>
				<li>Perianesthesia Nursing</li>
				<li>Perioperative Nursing</li>
				<li>Private Duty Nursing</li>
				<li>Public Health Nursing</li>
				<li>Pulmonary Nursing</li>
				<li>Quality Improvement</li>
				<li>Radiology Nursing</li>
				<li>Rehabilitation Nursing</li>
				<li>Research Nursing</li>
				<li>Renal Nursing</li>
				<li>School Nursing</li>
				<li>Space Nursing</li>
				<li>Sub-Acute Nursing</li>
				<li>Substance Abuse Nursing</li>
				<li>Surgical Nursing</li>
				<li>Telenursing</li>
				<li>Telephone Triage Nursing</li>
				<li>Transplantation Nursing</li>
				<li>Travel Nursing</li>
				<li>Urology Nursing</li>
				<li>Utilization Management</li>
				<li>Wound Care</li>
			</ul>
		  </div>
	  </div>
	</div>
  </section>
  
  <section class="slider-part">
	<div class="col-sm-12 col-md-12 col-xs-12 pade_none otr-slider">
      <div id="demo" class="slider_top_paddn">
        <div id="owl-demo2" class="owl-carousel">
          <div class="item"><a href="javascript:;"><img src="images/item-img-01.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-02.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-03.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-04.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-05.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-06.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-07.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-01.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-02.png" alt="Image"></a></div>
          <div class="item"><a href="javascript:;"><img src="images/item-img-03.png" alt="Image"></a></div>
        </div>
      </div>
    </div>
  </section>
</div>