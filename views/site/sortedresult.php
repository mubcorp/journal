<div class="table-cont">
	  <table>
		<tr class="items-head">
			<th class="numbering">No</th>
			<th class="name-id">Title</th>
			<th class="frequency">Frequency</th>
			<th class="periodicity">Periodicity</th>
			<th class="print">Format</th>
			<th class="inr">Currency</th>
			<th class="numeric">Price</th>
			<th class="exchange-rate">Exchange<br> Rate</th>
			<th class="numeric-2">INR Price</th>
			<th class="checked">Select</th>
		</tr>
		<tr class="table-heading">
			<td colspan="10">National Nursing Journals</td>
		</tr>
		<?php 
		foreach($journals['national'] as $key => $nJournal){
			?>
		<tr>
			<td class="numbering"><?=$key+1;?></td>
			<td class="name-id"><a href="/site/about?slug=<?=$nJournal->slug;?>#detailPage"><?=$nJournal->title;?></a></td>
			<td class="frequency"><?=$nJournal->frequency;?></td>
			<td class="periodicity"><?=$nJournal->periodicity;?></td>
			<td class="print"><?=$nJournal->format;?></td>
			<td class="inr"><?=$nJournal->currency;?></td>
			<td class="numeric"><?=$nJournal->price;?></td>
			<td class="exchange-rate"><?=$nJournal->exchange_rate;?></td>
			<td class="numeric-2"><?=$nJournal->inr_price;?></td>
			<td class="checked">
			  <div class="box_check_txt">
				<input type="checkbox" id="c_<?=$nJournal->id;?>" name="cc"/>
				<label class="btn-addcart" for="c_<?=$nJournal->id;?>"><span class="green"></span></label>
			  </div>
			</td>
		</tr>
		<?php }?>
	  <!--/table>
	  <table-->
		<tr class="table-heading">
			<td colspan="10">International Nursing Journals</td>
		</tr>
		<?php foreach($journals['international'] as $key => $intJournal){
			?>
		<tr>
			<td class="numbering"><?=$key+1;?></td>
			<td class="name-id"><a href="/site/about?slug=<?=$intJournal->slug;?>#detailPage"><?=$intJournal->title;?></a></td>
			<td class="frequency"><?=$intJournal->frequency;?></td>
			<td class="periodicity"><?=$intJournal->periodicity;?></td>
			<td class="print"><?=$intJournal->format;?></td>
			<td class="inr"><?=$intJournal->currency;?></td>
			<td class="numeric"><?=$intJournal->price;?></td>
			<td class="exchange-rate"><?=$intJournal->exchange_rate;?></td>
			<td class="numeric-2"><?=$intJournal->inr_price;?></td>
			<td class="checked">
			  <div class="box_check_txt">
				<input type="checkbox" id="i_<?=$intJournal->id;?>" name="cc" />
				<label class="btn-addcart" for="i_<?=$intJournal->id;?>"><span class="green"></span></label>
			  </div>
			</td>
		</tr>
		<?php }?>
	  </table>
</div>