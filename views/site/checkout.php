<?php
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    use yii\widgets\ActiveForm;
    use yz\shoppingcart\ShoppingCart;


    $cart = new ShoppingCart();
    $cartItems = $cart->getPositions();
    $count = count ( $cartItems );
    if(!empty($cartItems)){
    $restraunt = current($cartItems)->menu->restaurant;}

?>
<section class="featured-restaurants">
    <div class="container">
        <div class="row">
            <div class="restaurant-listing">
               <div class="col-xs-12 col-sm-12 col-md-12 single-restaurant grill thaifood pasta pizza">
                    <div class="restaurant-wrap">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-md-4"><b>Restaurant Name</b></div>   
                                    <!-- <div class="col-md-4"><b>Image</b></div> --> 
                                    <div class="col-md-4"><b>Item Name</b></div> 
                                    <div class="col-md-4"><b>Price</b></div> 
                                </div>
                            </div></div></div></div>
                <?php 
                    foreach($cartItems as $value){
                    $itemCartId = $value['id'];

                ?>
                <div class="col-xs-12 col-sm-12 col-md-12 single-restaurant grill thaifood pasta pizza">
                    <div class="restaurant-wrap">
                        <div class="row">
                            <!--end:col -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="col-md-4"><?= $restraunt->restaurant_name; ?></div>  
                                <div class="col-md-4"><?=$value['item_name']?></div> 
                                <div class="col-md-3">₹ <?=$value['price']?></div>
                                <div class="col-md-1"> <a class="btn-remove-item" id="remove_item_<?= $value['id'];?>"><i class="fa fa-trash pull-right"></i>
                                  </a></div> 
                            </div>
                            
                            <!-- end:col -->
                        </div>

                        <!-- end:row -->
                    </div>
                    <!--end:Restaurant wrap -->
                </div>
                <?php } ?>
                <div class="col-xs-12 col-sm-12 col-md-12 single-restaurant grill thaifood pasta pizza">
                    <div class="restaurant-wrap">
                        <div class="row"><div class="col-md-4"></div>
                            <div class="col-md-4">
                            <h5 style="margin-top: 10px;"><b>Total Amount: ₹ <?= $cart->getCost()?></b></h5></div>
                        <button class="payment">Make Payment</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>