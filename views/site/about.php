<?php 

	use yii\helpers\Html;
    use yii\widgets\ActiveForm;

	$journalDetail = new \app\modules\MubAdmin\modules\csvreader\models\Journals();
	$journalId = $journalDetails->id;
	$journalCat = $journalDetails->category;

	use yz\shoppingcart\ShoppingCart;

?>

<div class="col-xs-12 inner-page-content" id="detailPage">

  <section class="cont-img-table">
	<div class="container">
	  <div class="row">
		<div class="col-xs-12 pad-none table-img-part">
		  <div class="col-md-6 col-sm-6 col-xs-12 pad-none inner-img-part">
		    <div class="cont-image-ajn">
			  <div class="ajn-img"><img src="/web/images/ajn-img.png" alt="AJN Image"></div>
			  <div class="ajn-frame"><img src="/web/images/ajn-img-frame.png" alt="AJN Frame"></div>
		    </div>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12 pad-none inner-table-part">
			<div class="cont-inner-table">
				<table>
					<tr>
						<th>Title</th>
						<th><?= $journalDetails->title; ?></th>
					</tr>
					<tr>
						<td>Print ISSN</td>
						<td><?= $journalDetails->print_issn; ?></td>
					</tr>
					<tr>
						<td>Online ISSN</td>
						<td><?= $journalDetails->online_issn; ?></td>
					</tr>
					<tr>
						<td>Issues per year</td>
						<td><?= $journalDetails->frequency; ?></td>
					</tr>
					<tr>
						<td>Periodicity</td>
						<td><?= $journalDetails->periodicity; ?></td>
					</tr>
					<tr>
						<td>Language</td>
						<td><?= $journalDetails->language; ?></td>
					</tr>
					<tr>
						<td>Origin	</td>
						<td><?= $journalDetails->origin; ?></td>
					</tr>
					<tr>
						<td>Publisher</td>
						<td><?= $journalDetails->publisher; ?></td>
					</tr>
					<tr>
						<td>Estimated Delivery</td>
						<td>Delivered Time</td>
					</tr>
					<tr>
						<td>One Year Subscription</td>
						<td><?= $journalDetails->price; ?></td>
					</tr>
					<tr>
						<td>Format</td>
						<td><?= $journalDetails->format; ?></td>
					</tr>
				</table>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </section>
  
  <section class="cont-about-text">
	<div class="container">
	  <div class="row">
		<div class="col-xs-12 pad-none inner-page-heading">
		  <h2>About Journal</h2>
		  <p><span><?= $journalDetails->description; ?></span></p>
		</div>
		<div class="col-xs-12 pad-none print-outer">
			<div class="print-form">

				<?php $form = ActiveForm::begin([
                 'id' => 'newsletter'
                ]); ?>
			 <form method="post" id="newsletter-form" class="newsletter-form">
				<div class="email-field">
					<?= $form->field($signupMail, 'email')->textInput(['class' => 'email-field','placeholder' =>'Email Address'])->label(false);?>
				</div>

				<div class="email-submit email-page">
					 <input type="submit" value="Subscribe Us">
				</div>
			 </form>	<?php ActiveForm::end(); ?>

				<!-- <div class="email-submit print-page"><input type="button" value="Print" name="" placeholder="Email Address"></div> -->
			</div>
		</div>
		<div class="col-xs-12 pad-none addtolist-back-bttn">
			<div class="buttons-centr">
				<a href="/">Back To Home Page</a>

				<?php if($journalCat == 'NATIONAL'){?>

				<a href="/#cart" id="c_<?=$journalId;?>" ><label class="btn-addcart" for="c_<?=$journalId;?>">Add To List</label></a>
				<?php }else{?>

				<a href=/#cart id="i_<?=$journalId;?>"><label class="btn-addcart" for="i_<?=$journalId;?>">Add To List</label></a>
				<?php }?>

			</div>
		</div>
	  </div>
	</div>
  </section>
</div>

