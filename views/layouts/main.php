<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MubUser;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    
<header>
<?php if (\Yii::$app->session->hasFlash('success')): ?>
  <div class="alert alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button" id="Userregister"><h1><b>×</b></h1></button>
  <h4><i class="icon fa fa-check"></i>Mail Sent Successfully!</h4>
  <h5><?= \Yii::$app->session->getFlash('success') ?></h5>
  </div>
<?php endif; ?>
  <div class="col-xs-12 pad-none cont-header">
  <div class="col-xs-6 col-sm-6 col-xs-6 pad-none header-left">
    <div class="header-content-midd">
    <div class="col-xs-12 pad-none header-left-content">
      <div class="col-xs-12 pad-none catalog-heading">Catalog</div>
      <h1>Nursing</h1>
      <div class="col-xs-12 pad-none journal-magazine">Journals &amp; Magazines</div>
      <div class="col-xs-12 pad-none year-in-header">2018</div>
      <div class="col-xs-12 pad-none easy-steps">
      <h3>Easy Steps!!</h3>
      <ul>
        <li>View</li>
        <li>Select</li>
        <li>Shortlist</li>
        <li>Email</li>
        <li>Get Quote</li>
        <li>Approve</li>
        <li>Subscribe</li>
        <li>Renew</li>
      </ul>
      </div>
    </div>
    </div>
  </div>
  <div class="col-xs-6 col-xs-6 col-xs-6 pad-none header-right">
    <div class="banner-girl"><img src="/images/nurse-img.png" alt="Nurse Image"></div>
    <div class="cont-phone-mail">
      <div class="contact-info-otr">
        <div class="contact-icons phone-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
        <div class="contact-text">
                  <div class="midd-txt">
                  <a href="callto:9773723591">9773723591</a><span>/</span><a href="callto:9773723592">9773723592</a>
                  </div>
                </div>
      </div>
      <div class="contact-info-otr">
        <div class="contact-icons mail-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
        <div class="contact-text">
                  <div class="midd-txt">
                    <a href="mailto:Subscribe@NursingJournals.Online">Subscribe@NursingJournals.Online</a>
                  </div>
                </div>
      </div>
    </div>
  </div>
  </div>
</header>

    <?= $content ?>
<footer class="cont-footer">
  <section class="footer-content">
    <div class="col-xs-12 pade_none address-footer-outer">
    <p>Knowledge Tree | Salarpuria Sattva Knowledge City | Unit 2 . Sy.No. 83/1 . Plot No-2 | Inorbit Mall Road | Raidurg Village | HITEC City | Hyderabad 500081 | Telangana | India</p>
    </div>    
    <div class="col-xs-12 pade_none services-footer-outer">
      <div class="col-xs-12 pade_none contact-footr footer-number-otr">Customer Service - <a href="callto:9773723591">9773723591</a> / <a href="callto:9773723592">9773723592</a></div>
      <div class="col-xs-12 pade_none contact-footr footer-email-otr"><a href="mailto:">contact@NursingJournals.Online</a></div>
    </div>
  </section>
<footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>