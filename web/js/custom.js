$(document).ready(function() {
  $("#owl-demo").owlCarousel({
    navigation : true
  });
  $("#owl-demo2").owlCarousel({
    navigation : true
  });
});

$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
});