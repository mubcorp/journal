<?php

namespace app\models;

use Yii;
use app\components\Model;

class SignupMail extends Model
{
    public static function tableName()
    {
        return 'signupmail';
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
        ];
    }

    public function attributeLabels()
    {
        return [
            
            'email' => 'Email',

        ];
    }
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo('vikkumar648@gmail.com')
                ->setFrom('admin@makeubig.com')
                ->setSubject('Subscriber')
                ->setTextBody('<b>Hello</b>')
                ->send();

            return true;
        }
        return false;
    }
}
