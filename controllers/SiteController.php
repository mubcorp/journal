<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\MubUser;
use app\models\ClientSignup;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\MubAdmin\modules\csvreader\models\Journals;
use yz\shoppingcart\ShoppingCart;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccess'],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (
                $action->id == "payment-process" || $action->id == 'payment-cancel'
        ) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $journals = new Journals();
        $contact = new \app\models\ContactForm();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $contact->load($params))
        {
            $this->layout = false;
            if($contact->save(false))
            { 
                $sent = $this->sendMail($contact);
                if($sent)
                {
                    \Yii::$app->session->setFlash('success', "Your query was received Successfully!");
                    return $this->redirect('/');
                }
                else
                {
                    \Yii::$app->session->setFlash('error', "There was an error in sending Mail Right Now!");
                    return $this->redirect('/');
                }
            }
            else
            {
                p($contact->getErrors());
            }
        }

        $national = $journals::find()->where(['del_status' => '0','category' => 'NATIONAL'])->all();
        $international = $journals::find()->where(['del_status' => '0','category' => 'INTERNATIONAL'])->all();
        $desNational = $journals::find()->where(['del_status' => '0','category' => 'NATIONAL'])->orderBy(['title'=>SORT_DESC])->all();
        $desInternational = $journals::find()->where(['del_status' => '0','category' => 'INTERNATIONAL'])->orderBy(['title'=>SORT_DESC])->all();
        $priceNational = $journals::find()->where(['del_status' => '0','category' => 'NATIONAL'])->orderBy(['inr_price'=>SORT_DESC])->all();
        $priceInternational = $journals::find()->where(['del_status' => '0','category' => 'INTERNATIONAL'])->orderBy(['inr_price'=>SORT_DESC])->all();
        $priceLowNational = $journals::find()->where(['del_status' => '0','category' => 'NATIONAL'])->orderBy(['inr_price'=>SORT_ASC])->all();
        $priceLowInternational = $journals::find()->where(['del_status' => '0','category' => 'INTERNATIONAL'])->orderBy(['inr_price'=>SORT_ASC])->all();

        $activeJournals = ['national' => $national,'international' => $international, 'desNational' => $desNational, 'desInternational' => $desInternational, 'priceNational' => $priceNational,'priceInternational' => $priceInternational,'priceLowNational' => $priceLowNational,'priceLowInternational' => $priceLowInternational];
        return $this->render('index',['journals' => $activeJournals,'contact' => $contact]);
    }

    public function sendMail($contact)
    {
        $cart = new ShoppingCart();
        $cartItems = ArrayHelper::toArray($cart->getPositions());
        $price = $cart->getCost();
        $success = \Yii::$app->mailer->compose('booking',['cartItems' => $cartItems, 'price' => $price])
          ->setFrom('contact@nursingjournals.online')
          ->setTo('contact@nursingjournals.online.test-google-a.com')
          ->setSubject('Your Journal Details')
          ->setTextBody('Plain text content')
          ->setCc($contact->email)
          ->setBcc('praveen@makeubig.com')
          ->send();
        return $success;
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/mub-admin/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $mubUser = new \app\models\MubUser();
        $mubUserId = \app\models\User::getMubUserId();  
        $currentRole = $mubUser::findOne($mubUserId)['role'];
            if($currentRole == 'client') 
            {
             Yii::$app->user->logout();
                return $this->goHome(); 
            }
            else
            {
            Yii::$app->user->logout();
            return $this->redirect('/mub-admin/');
            }
    }

    /**
     * Displays about page.
     *
     * @return string
     */

    public function actionSuccesspdf($id)
    {
        $this->layout = false;
        $pdf = \Yii::$app->pdf;
        $pdf->content = $this->renderPartial('successpdf');
        return $pdf->render();
    }

    public function actionGetItems($menuId)
    {
        $this->layout = false;
        $getItem = new Journals();
        $menuDetail = $getItem::find()->where(['menu_id' => $menuId, 'del_status' => '0'])->all();
        $response = $this->render('items',['items' => $menuDetail]);
       return $response;
    }

    public function actionAddtocart($id)
    {   
        $cart = new ShoppingCart();
        $this->layout = false;
        $model = Journals::findOne($id);
        if ($model){
            $cart->put($model, 1);
            return $this->render('addtocart', [
                'cart' => $cart
            ]);
        }
        throw new NotFoundHttpException();
    }

    public function actionClearCart($id=NULL)
    {
         $cart = new ShoppingCart();
         $this->layout = false;
         if($id == NULL)
         {
            $cart->removeAll();
         }
         else
         {
            $cart->removeById($id);  
         }
         return $this->render('showcart', [
            'cart' => $cart]);
    }

    public function actionShowcart()
    {
        $this->layout = false;
        $cart = new ShoppingCart();
        $items = $cart->getPositions();
        return $this->render('showcart', [
            'items' => $items]);  
    }

    public function actionUpdateCartItems($id,$quantity)
    {  
        if($quantity >= 0)
        {
            $cart = new ShoppingCart();
            $this->layout = false;
            $model = Journals::findOne($id);
            if($model)
            {
                $cart->update($model, $quantity);
            }
            $total = $cart->getCost(); 
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $data['total'] = $total;
            return $data;
        } 
    }

    public function actionAbout($slug)
    {
        $signupMail = new \app\models\SignupMail();
        $journal = new \app\modules\MubAdmin\modules\csvreader\models\Journals();
        $journalDetails = $journal::find()->where(['slug' => $slug])->one();
        return $this->render('about',['journalDetails' => $journalDetails,'signupMail' => $signupMail]);
    }

    public function actionSignup()
    {
        $params = \yii::$app->request->getBodyParams();
        $signupMail = new \app\models\SignupMail();
        if($signupMail->load($params))
         {
          if ($signupMail->save())
          {
             \Yii::$app->mailer->compose('subscribe')
                ->setTo('praveen@makeubig.com')
                ->setBcc('vikkumar648@gmail.com')
                ->setFrom(['admin@makeubig.com' => 'Journals'])
                ->setSubject('Customer Signed Up')
                ->setHtmlBody("<b>A client with Mail Id ".$signupMail->email." Signed up to your Newsletter")
                ->send();    
            return true;
            }
            else
            {
            return false;
            }
        }
    }

}

