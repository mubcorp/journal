<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\MubUser;
use app\models\MubUserSearch;

class DashboardController extends MubController
{
	public function getProcessModel()
	{
        return new BlogProcess();
	}

	public function getPrimaryModel()
	{
        return new MubUser();
	}

    public function getSearchModel()
    {
        return new MubUserSearch();
    }

	public function actionImageUpload()
	{
		$file = \Yii::$app->getBodyParams();
		p($file);
	}

	public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }
}