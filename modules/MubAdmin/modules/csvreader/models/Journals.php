<?php

namespace app\modules\MubAdmin\modules\csvreader\models;

use Yii;
use yz\shoppingcart\CartPositionTrait;
use yz\shoppingcart\CartPositionInterface;

/**
 * This is the model class for table "journals".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $category
 * @property string $print_issn
 * @property string $online_issn
 * @property string $frequency
 * @property string $periodicity
 * @property string $format
 * @property string $language
 * @property string $origin
 * @property string $currency
 * @property string $price
 * @property string $exchange_rate
 * @property string $inr_price
 * @property string $publisher
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 */
class Journals extends \app\components\Model implements CartPositionInterface
{
    use CartPositionTrait;

    public function getPrice()
    {
        return $this->inr_price;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function tableName()
    {
        return 'journals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'frequency', 'periodicity', 'format', 'language', 'origin', 'currency', 'price', 'exchange_rate', 'inr_price', 'publisher'], 'required'],
            [['title','slug','category', 'description', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['format', 'currency', 'price', 'exchange_rate', 'inr_price'], 'string', 'max' => 255],
            [['print_issn', 'online_issn', 'frequency', 'periodicity', 'language', 'origin', 'publisher'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'category' => 'Category',
            'print_issn' => 'Print Issn',
            'online_issn' => 'Online Issn',
            'frequency' => 'Frequency',
            'periodicity' => 'Periodicity',
            'format' => 'Format',
            'language' => 'Language',
            'origin' => 'Origin',
            'currency' => 'Currency',
            'price' => 'Price',
            'exchange_rate' => 'Exchange Rate',
            'inr_price' => 'Inr Price',
            'publisher' => 'Publisher',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }
}
