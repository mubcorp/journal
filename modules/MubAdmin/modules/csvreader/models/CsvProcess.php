<?php 

namespace app\modules\MubAdmin\modules\csvreader\models;
use app\modules\MubAdmin\modules\csvreader\models\Journals;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class CsvProcess extends Model
{
	  public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $this->models = [''];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [];
        return $this->relatedModels;
    }


    public function saveJournals($data)
    {
      $journalsModel = new Journals();
      if($journalsModel->load($data) && $journalsModel->validate())
        { 
        $slug = $data['Journals']['slug'];
        $exists = $journalsModel::find()->where(['slug' => $slug])->one();
        if($exists)
        { 
          p($data);
          $response = $journalsModel->updateAll($data['Journals'],['journals.id' => $exists->id]);
          //p(['rows_updated' => $response]);
        }
        else
        { 
          if(!$journalsModel->save())
          {   
              p($journalsModel->getErrors());
          }
          return true;
        }
      }
      else
      { 
        p($journalsModel->getErrors());
      }
    }

    public function saveData($csvFile)
    {   
        if(!empty($csvFile))
        {
           //unset($csvFile[0]);
           foreach ($csvFile as $rowKey => $row) {
               $dataSet = explode(',',$row);
               if($dataSet[0] != '')
               {
                $pubMask = (isset($dataSet[11])) ? $dataSet[11] : 'NA';
                $slugMask = $dataSet[0]."-".$pubMask;
                  $data['Journals']['title'] = (isset($dataSet[0]) && ($dataSet[0] !== ''))?str_replace('_', ',', $dataSet[0]) :'NA';
                 $data['Journals']['slug'] = \app\helpers\StringHelper::generateSlug($slugMask);
                 $data['Journals']['category'] = (isset($dataSet[1]) && ($dataSet[1] !== ''))? $dataSet[1] : 'NA';
                 $data['Journals']['print_issn'] = (isset($dataSet[2]) && ($dataSet[2] !== ''))?$dataSet[2] :'NA';
                 $data['Journals']['online_issn'] = (isset($dataSet[3]) && ($dataSet[3] !== ''))?$dataSet[3] :'NA';
                 $data['Journals']['frequency'] = (isset($dataSet[4]) && ($dataSet[4] !== ''))?$dataSet[4] :'NA';
                 $data['Journals']['periodicity'] =  (isset($dataSet[5]) && ($dataSet[5] !== ''))?$dataSet[5] :'NA';
                 $data['Journals']['format'] =  'Print';
                 $data['Journals']['language'] = 'English';
                 $data['Journals']['origin'] =  (isset($dataSet[6]) && ($dataSet[6] !== ''))?$dataSet[6] :'NA';
                 $data['Journals']['currency'] =  (isset($dataSet[7]) && ($dataSet[7] !== ''))?$dataSet[7] :'NA';
                 $data['Journals']['price'] =  (isset($dataSet[8]) && ($dataSet[8] !== ''))?$dataSet[8] :'NA';
                 $data['Journals']['exchange_rate'] = (isset($dataSet[9]) && ($dataSet[9] !== ''))?$dataSet[9] :'NA';
                 $data['Journals']['inr_price'] =  (isset($dataSet[10]) && ($dataSet[10] !== ''))?$dataSet[10] :'NA';
                 $data['Journals']['publisher'] =  (isset($dataSet[11]) && ($dataSet[11] !== ''))?str_replace('_', ',', $dataSet[11]) :'NA';
                 $data['Journals']['description'] =  (isset($dataSet[12]) && ($dataSet[12] !== ''))?str_replace('_', ',', $dataSet[12]) :'NA';
                 if($data['Journals']['category'] != 'NA')
                 {
                   $journalsCount = $this->saveJournals($data); 
                 }
               }
            } 
        }
    }
}